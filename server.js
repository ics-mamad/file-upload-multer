const express = require('express')
const app = express()
const Router = require('./Routes/UploadRoute')

app.use('/api/',Router)

const port = 3001

app.listen(port,()=>{
    console.log(`Server Running at ${port}`)
})