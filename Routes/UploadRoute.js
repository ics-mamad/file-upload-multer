const express = require('express')
const Router = express.Router();
const multer = require('multer')
const {singleUpload,multipleUpload,upload} = require('../Controllers/UploadController')

Router.post("/upload/single",upload.single('file'), singleUpload)
Router.post("/upload/multiple",upload.array("file", 3), multipleUpload)

module.exports = Router

  

