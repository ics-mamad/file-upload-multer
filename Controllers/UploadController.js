const multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads/")
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`)
  },
})

const maxSize = 5 * 1024 * 1024 //5 MB

const upload = multer({ 
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "application/pdf") { //for images :- image/png or image/jpg or image/png
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .pdf format allowed!'));
        }},
    limits:()=> {fileSize:maxSize},
    storage    
})


const singleUpload = (req,res) =>{
    res.send(req.file)
}

const multipleUpload = (req,res)=>{
  res.send(req.files)
}

module.exports = {singleUpload, multipleUpload, storage,upload}